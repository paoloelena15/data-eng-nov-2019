package com.scling.exercises.simple

import com.scling.exercises.Order
import com.scling.exercises.Serde._
import com.scling.tinybigdata.Coll

// Read a dataset with orders. Some orders have invalid fields. Drop the orders whose id or item fields are empty
// strings. For orders with empty strings as users, replace with <unknown_user>.


// Example input and output (copied from WashJobTest.scala):
/*
    val input = Seq(

      // This is a correct order, which will be included in the output.
      Order("1", "alice", "pants"),

      // This order has no user. The user field will be replaced with <unknown_user> in the output.
      Order("2", "", "pants"),

      // This order has no order id, and is invalid. It will be dropped in the output.
      Order("", "cecilia", "pants"),

      // This order has no item, and will also be dropped.
      Order("3", "bob", ""))


    val expectedOutput = Seq(

      // The correct order
      Order("1", "alice", "pants"),

      // The record with the mended user.
      Order("2", "<unknown_user>", "pants"))

*/


object WashJob {
  def main(args: Array[String]): Unit = {
    val inputFile = args(1)
    val outputFile = args(2)

    val input = Coll.readJson[Order](inputFile)
    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[Order] = Coll.empty[Order]
    result.writeJson(outputFile)
  }
}
