package com.scling.exercises.simple

import com.scling.exercises.Serde._
import com.scling.exercises._
import com.scling.tinybigdata.Coll

// Read a dataset with orders. Join with users and count the number of orders per country.

object CountryCountJob {
  def main(args: Array[String]): Unit = {
    val orderPath = args(1)
    val userPath = args(2)
    val outputPath = args(3)

    val order = Coll.readJson[Order](orderPath)
    val user = Coll.readJson[User](userPath)

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[CountryCount] = Coll.empty[CountryCount]
    result.writeJson(outputPath)
  }
}
