package com.scling.exercises.intermediate

import com.github.nscala_time.time.Imports._
import com.scling.exercises.Serde._
import com.scling.exercises._
import com.scling.tinybigdata.Coll
import org.joda.time.PeriodType
import org.joda.time.format.ISODateTimeFormat

// Read datasets with PageViews and User data. Emit a list of Swedish users that have not been active in n days,
// in order to send them nag mail. Due to a faulty client library, some PageView events have trailing white space
// in the user field. Users without any activity at all should not get a mail.
//
// The current date and the age limit in days are passed as command line arguments to the job.

object InactiveUsersJob {
  def main(args: Array[String]): Unit = {
    val date = ISODateTimeFormat.date().parseDateTime(args(1))
    val ageDays = args(2).toInt
    val pageViewPath = args(3)
    val userPath = args(4)
    val outputPath = args(5)

    val pageView = Coll.readJson[PageView](pageViewPath)
    val user = Coll.readJson[User](userPath)

    def isOld(pv: PageView): Boolean = {
      val interval: Interval = pv.time to date
      val period: Period = interval.toPeriod(PeriodType.days())
      val days: Int = period.days
      days > ageDays
    }

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[UserLastActive] = Coll.empty[UserLastActive]

    result.writeJson(outputPath)
  }
}


