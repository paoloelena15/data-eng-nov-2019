package com.scling.exercises.intermediate

import com.scling.exercises._
import com.scling.tinybigdata.Coll


// Given a dataset with page rendering events, containing web page URLs and render times, calculate the
// 1%, 5%, 10%, 50%, 90%, 95%, 99% percentiles for rendering times per URL.

object RenderPercentilesJob {

  // Helper function: From a sorted vector of values, get the value at the nth percentile.
  def percentile(percent: Int, sortedMillis: Vector[Int]): Int = {
    sortedMillis((percent / 100.0 * sortedMillis.size).toInt)
  }

  def main(args: Array[String]): Unit = {
    import Serde._

    val pageRenderPath = args(1)
    val outputPath = args(2)

    val pageRender = Coll.readJson[PageRender](pageRenderPath)

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[RenderPercentiles] = Coll.empty[RenderPercentiles]
    result.writeJson(outputPath)
  }
}
