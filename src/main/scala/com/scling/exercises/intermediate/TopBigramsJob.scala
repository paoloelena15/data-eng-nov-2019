package com.scling.exercises.intermediate

import com.scling.exercises.{BigramCount, MessagePost}
import com.scling.tinybigdata.Coll
import play.api.libs.json._

import scala.annotation.tailrec

// A bigram is a pair of words next to each other in a text. Read datasets with messages (e.g. posted to a forum) and
// emit a dataset with the n most popular bigrams, with counts.

object TopBigramsJob {
  def main(args: Array[String]): Unit = {
    implicit val postReads: Reads[MessagePost] = Json.reads[MessagePost]
    implicit val bigramWrites: Writes[BigramCount] = Json.writes[BigramCount]

    val topCount = args(1).toInt
    val inputFile = args(2)
    val outputFile = args(3)

    val posts = Coll.readJson[MessagePost](inputFile)

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


  val result: Coll[BigramCount] = Coll.empty[BigramCount]
  result.writeJson(outputFile)
  }
}
