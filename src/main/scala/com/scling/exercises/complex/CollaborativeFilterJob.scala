package com.scling.exercises.complex

import com.scling.exercises._
import com.scling.tinybigdata.Coll


// Read a dataset with page (movie) view events, containing web page URLs, user, and time stamps. For each user, figure
// out the user with most similar taste. For each user, lind a movie that the most similar user has viewed, but the
// user has not. Emit one recommendation for each user. Hint: use cosine similarity or Jaccard similarity for
// comparing user taste.

object CollaborativeFilterJob {

  def main(args: Array[String]): Unit = {
    import Serde._

    val pageViewPath = args(1)
    val outputPath = args(2)

    val pageView = Coll.readJson[PageView](pageViewPath)

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[Recommendation] = Coll.empty[Recommendation]
    result.writeJson(outputPath)
  }
}

case class ViewHistory(user: String, viewCounts: Map[String, Long]) {
  def increase(url: String): ViewHistory = {
    val oldVal: Long = viewCounts.getOrElse(url, 0)
    copy(viewCounts = viewCounts.updated(url, oldVal + 1L))
  }
}


case class TasteSimilarity(profile: ViewHistory, other: ViewHistory) {
  def sameUser: Boolean = profile.user == other.user

  def cosineSimilarity: Double =
    dotProduct / (magnitude(profile) * magnitude(other))

  private def dotProduct = {
    val urls = profile.viewCounts.keySet ++ other.viewCounts.keySet // Don't actually need both, since we do a dot product.
    urls.map(url => profile.viewCounts.getOrElse(url, 0L) * other.viewCounts.getOrElse(url, 0L)).sum
  }

  private def magnitude(history: ViewHistory) =
    Math.sqrt(history.viewCounts.values.map(v => v * v).sum)

  def recommendation: Option[Recommendation] = {
    val notSeen = other.viewCounts.keySet -- profile.viewCounts.keySet
    // Pick an arbitrary not seen url from neighbour.
    for {
      url <- notSeen.headOption
    } yield Recommendation(profile.user, url)
  }
}
