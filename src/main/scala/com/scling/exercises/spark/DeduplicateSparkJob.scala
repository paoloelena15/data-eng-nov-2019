package com.scling.exercises.spark

import com.scling.exercises.Order
import org.apache.spark.sql.{Dataset, SparkSession}

// Read a dataset with orders. Some orders are duplicates, and have identical id fields. Remove duplicates and ensure
// that each order has a unique id.

object DeduplicateSparkJob {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[1]").getOrCreate()
    run(args, spark)
  }

  def run(args: Array[String], spark: SparkSession): Unit = {
    import spark.implicits._

    val inputFile = args(1)
    val outputFile = args(2)

    val input: Dataset[Order] = spark.read.json(inputFile).as[Order]

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Dataset[Order] = ???
    result.write.json(outputFile)
  }
}
