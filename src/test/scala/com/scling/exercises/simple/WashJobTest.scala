package com.scling.exercises.simple

import better.files.File
import com.scling.exercises.{BatchJobTestRunner1, Order, Serde}
import org.scalatest.FunSuite

class WashJobTest extends FunSuite with BatchJobTestRunner1[Order, Order] {

  import Serde._

  implicit val orderOrdering: Ordering[Order] = Ordering.by(Order.unapply)

  override def runJob(inputPath: File, outputPath: File): Unit =
    WashJob.main(Array("WashJob", inputPath.toString, outputPath.toString))

  ignore("Example") {
    val input = Seq(

      // This is a correct order, which will be included in the output.
      Order("1", "alice", "pants"),

      // This order has no user. The user field will be replaced with <unknown_user> in the output.
      Order("2", "", "pants"),

      // This order has no order id, and is invalid. It will be dropped in the output.
      Order("", "cecilia", "pants"),

      // This order has no item, and will also be dropped.
      Order("3", "bob", ""))


    val expectedOutput = Seq(

      // The correct order
      Order("1", "alice", "pants"),

      // The record with the mended user.
      Order("2", "<unknown_user>", "pants"))

    assert(runTest(input) === expectedOutput)
  }


  test("Empty input") {
    assert(runTest(Seq()) === Seq())
  }

  ignore("Single correct order") {
    val input = Seq(Order("1", "alice", "pants"))
    assert(runTest(input) === input)
  }

  ignore("No id") {
    val input = Seq(Order("", "alice", "pants"))
    assert(runTest(input) === Seq())
  }

  ignore("No item") {
    val input = Seq(Order("1", "alice", ""))
    assert(runTest(input) === Seq())
  }

  ignore("No user") {
    val input = Seq(Order("1", "", "pants"))
    assert(runTest(input) === Seq(Order("1", "<unknown_user>", "pants")))
  }
}

