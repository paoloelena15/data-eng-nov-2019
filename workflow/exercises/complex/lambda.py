from datetime import timedelta

from luigi import DateHourParameter, IntParameter
from luigi import ExternalTask
from luigi.contrib.external_program import ExternalProgramTask
from luigi.task import WrapperTask

from lib.coordinates import LAKE
from lib.local_target import LocalFlagTarget


class PageviewCleanFastAndSlow(WrapperTask):
    """Build deduplicated and time shuffled pageview datasets, using dedup_pageview.py.

    One job should be fast, and only look at the designated hour, whereas one job should be slow, and wait for
    another three hours to see if any events are arriving late, and then output a more complete dataset.
    """

    hour = DateHourParameter()

    # Insert solution code here. Most Luigi tasks need one or more of:
    # job parameters (date, etc), requires() method, and output() or complete() method,
    # as well as methods specific to type of task, e.g. program_args (ExternalProgramTask), database (CopyToTable), ...


