from pathlib import Path

from luigi import DateParameter, Parameter
from luigi import ExternalTask
from luigi.contrib.external_program import ExternalProgramTask
from luigi.contrib.mysqldb import CopyToTable

from lib.coordinates import LAKE
from lib.dataset import read_json_lines
from lib.local_target import LocalFlagTarget


class SalesStatsEgress(CopyToTable):
    """Statistics for sales transactions. One row per day, containing date, num_items, num_users.

    Use sales_stats.py to calculate basic statistics over one day of sales data. Emit the stats to a
    tiny dataset in the lake. Then copy this dataset into a MySQL database instance.

    The test harness will set up MySQL in a container for you if you have docker installed on your
    machine. This test requires docker. It has not been tested on windows, and may very well fail
    due to line termination issues. You may try to change coordinates.MYSQL_DOCKER and start MySQL manually.
    """

    date = DateParameter()

    # significant=False tells Luigi to ignore this parameter in the unique id of the task. It is used when the
    # parameter value does not affect the output. It is debatable whether to use it for the host parameter.
    host = Parameter(significant=False)

    # Insert solution code here. Most Luigi tasks need one or more of:
    # job parameters (date, etc), requires() method, and output() or complete() method,
    # as well as methods specific to type of task, e.g. program_args (ExternalProgramTask), database (CopyToTable), ...


