from luigi import DateParameter, DateHourParameter
from luigi import ExternalTask
from luigi.contrib.external_program import ExternalProgramTask

from lib.coordinates import LAKE
from lib.local_target import LocalFlagTarget


class DemographicHourlySales(ExternalProgramTask):
    """Read sales data each hour. Join them with the corresponding daily user dump, using sales_user_join.py."""

    # Insert solution code here. Most Luigi tasks need one or more of:
    # job parameters (date, etc), requires() method, and output() or complete() method,
    # as well as methods specific to type of task, e.g. program_args (ExternalProgramTask), database (CopyToTable), ...


