import itertools
import json
import shutil


def read_json_lines(path):
    if not (path / '_SUCCESS').exists():
        raise FileNotFoundError(f'No dataset in {path}, _SUCCESS file missing')
    json_files = path.glob('*.json')
    data = [json.loads(line)
            for f in json_files
            for line in f.read_text().splitlines()]
    return data


def write_json_lines(path, data):
    if path.exists():
        shutil.rmtree(str(path))
    path.mkdir(parents=True)
    lines = [json.dumps(j) for j in data]
    (path / 'part-00000.json').write_text('\n'.join(lines + ['']))
    (path / '_SUCCESS').write_text('')


def dict_join(left, left_key, right, right_key):
    """Poor man's inner join of datasets, represented as List[dict]."""
    result = []
    for left_rec in left:
        key = left_rec[left_key]
        for right_rec in right:
            if right_rec[right_key] == key:
                match = dict(left_rec)
                match.update(right_rec)
                # Remove the redundant key field
                del match[right_key]
                result.append(match)
    return result

def dict_group_by(data, key):
    """Group data by a certain key."""
    by_id = sorted(data, key=lambda r: r[key])
    return [(k, list(gr)) for k, gr in itertools.groupby(by_id, key=lambda r: r[key])]
