# noinspection PyUnresolvedReferences
import json
from pathlib import Path
# noinspection PyUnresolvedReferences
from unittest import TestCase, skip

from lib.coordinates import TEST_LAKE
from test.harness import run_luigi, write_dataset, read_dataset, DbWorkflowTestcase


class DbEgressTest(DbWorkflowTestcase):
    def setUp(self):
        super().setUp()
        self.db_execute('drop table if exists sales_stats')

    def run_job(self, d):
        run_luigi('exercises.complex.db_egress', 'SalesStatsEgress', host=self.db_host, date=d)

    @skip("Remove this line to enable the test")
    def test_happy(self):
        sales_path = Path(f'{TEST_LAKE}/cold/SalesDaily/year=2019/month=10/day=13')
        write_dataset(sales_path,
                      [{'item': 'shoe', 'price': 7, 'user': 'alice'},
                       {'item': 'shoe', 'price': 7, 'user': 'bob'},
                       {'item': 'pants', 'price': 12, 'user': 'cecilia'}])

        self.run_job('2019-10-13')
        sum_path = Path(f'{TEST_LAKE}/derived/SalesStatsDaily/year=2019/month=10/day=13')
        output = read_dataset(sum_path)
        self.assertEqual([{'date': '2019-10-13', 'num_items': 2, 'num_users': 3}], output)

        db_output = self.get_db_rows('sales_stats')
        self.assertEqual([{'date': '2019-10-13', 'num_items': 2, 'num_users': 3}], db_output)


        sales_14_path = Path(f'{TEST_LAKE}/cold/SalesDaily/year=2019/month=10/day=14')
        write_dataset(sales_14_path,
                      [{'item': 'pants', 'price': 7, 'user': 'alice'},
                       {'item': 'pants', 'price': 12, 'user': 'bob'}])

        self.run_job('2019-10-14')

        db_output = self.get_db_rows('sales_stats')
        self.assertEqual([{'date': '2019-10-13', 'num_items': 2, 'num_users': 3},
                          {'date': '2019-10-14', 'num_items': 1, 'num_users': 2}], db_output)
