# noinspection PyUnresolvedReferences
from pathlib import Path
# noinspection PyUnresolvedReferences
from unittest import TestCase, skip

from lib.coordinates import TEST_LAKE
from test.harness import run_luigi, write_dataset, read_dataset, WorkflowTestCase


class DemographicHourlySalesTest(WorkflowTestCase):
    def run_job(self, h):
        run_luigi('exercises.simple.demographic_hourly_sales', 'DemographicHourlySales', hour=h)

    @skip("Remove this line to enable the test")
    def test_happy(self):
        sales_path = Path(f'{TEST_LAKE}/cold/SalesHourly/year=2019/month=10/day=13/hour=09')
        write_dataset(sales_path,
                      [{'item': 'shoe', 'price': 5, 'user': 'alice'},
                       {'item': 'socks', 'price': 2, 'user': 'bob'}])
        user_path = Path(f'{TEST_LAKE}/cold/UserSnapshot/year=2019/month=10/day=13')
        write_dataset(user_path,
                      [{'name': 'alice', 'email': 'alice@gmail.com'},
                       {'name': 'bob', 'email': 'bob@gmail.com'}])

        output_path = Path(f'{TEST_LAKE}/derived/DemographicSalesHourly/year=2019/month=10/day=13/hour=09')

        self.run_job('2019-10-13T09')
        output = read_dataset(output_path)
        # Verify that we got data from both input datasets.
        self.assertCountEqual(
            [{'item': 'shoe', 'price': 5, 'user': 'alice', 'email': 'alice@gmail.com'},
             {'item': 'socks', 'price': 2, 'user': 'bob', 'email': 'bob@gmail.com'}], output)
