# noinspection PyUnresolvedReferences
import json
from pathlib import Path
# noinspection PyUnresolvedReferences
from unittest import TestCase, skip

from lib.coordinates import TEST_LAKE
from test.harness import run_luigi, write_dataset, read_dataset, WorkflowTestCase, write_raw_dataset


class DayAggregateTest(WorkflowTestCase):
    def run_job(self, d):
        run_luigi('exercises.simple.sales_aggregate', 'SalesAggregateDaily', date=d)

    @skip("Remove this line to enable the test")
    def test_happy(self):
        for h in range(24):
            sales_path = Path(f'{TEST_LAKE}/cold/SalesHourly/year=2019/month=10/day=13/hour={h:02}')
            write_dataset(sales_path,
                          [{'item': 'shoe', 'price': 5 + h, 'user': 'alice'}])

        output_path = Path(f'{TEST_LAKE}/derived/SalesAggregateDaily/year=2019/month=10/day=13')

        self.run_job('2019-10-13')
        output = read_dataset(output_path)
        # Verify that we got data from all input datasets.
        self.assertEqual(list(range(5, 29)), sorted([r['price'] for r in output]))

    @skip("Remove this line to enable the test")
    def test_one_missing(self):
        for h in range(1, 24):
            sales_path = Path(f'{TEST_LAKE}/cold/SalesHourly/year=2019/month=10/day=13/hour={h:02}')
            write_dataset(sales_path,
                          [{'item': 'shoe', 'price': 5 + h, 'user': 'alice'}])

        output_path = Path(f'{TEST_LAKE}/derived/SalesAggregateDaily/year=2019/month=10/day=13')

        self.run_job('2019-10-13')
        output = read_dataset(output_path)
        self.assertIsNone(output)

    @skip("Remove this line to enable the test")
    def test_one_broken(self):
        for h in range(23):
            sales_path = Path(f'{TEST_LAKE}/cold/SalesHourly/year=2019/month=10/day=13/hour={h:02}')
            write_dataset(sales_path,
                          [{'item': 'shoe', 'price': 5 + h, 'user': 'alice'}])

        # One dataset is broken. We should attempt to run, but the execution will fail.
        broken_sales_path = Path(f'{TEST_LAKE}/cold/SalesHourly/year=2019/month=10/day=13/hour=23')
        write_raw_dataset(broken_sales_path,
                          json.dumps({'item': 'shoe', 'price': 5 + 23, 'user': 'alice'})[:-5])

        output_path = Path(f'{TEST_LAKE}/derived/SalesAggregateDaily/year=2019/month=10/day=13')

        self.run_job('2019-10-13')
        output = read_dataset(output_path)
        self.assertIsNone(output)
